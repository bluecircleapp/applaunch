package com.kvng_willy.applaunch.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kvng_willy.applaunch.R
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket
import com.kvng_willy.applaunch.data.network.ShoppingList
import com.kvng_willy.applaunch.data.repositories.DataRepository
import com.kvng_willy.applaunch.util.Coroutines
import java.util.*
import kotlin.collections.ArrayList

class Shopping2Adapter(private val context: Context, private val shoppingList:ArrayList<ShoppingBasket>,private val repository: DataRepository):BaseAdapter() {
    private var mRecyclerViewItems: ArrayList<ShoppingBasket>? = shoppingList
    private var mRecyclerViewItems1: ArrayList<ShoppingBasket>? = shoppingList
    private var bugsFiltered: MutableList<ShoppingBasket> = ArrayList()
    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount(): Int {
        return shoppingList.size
    }

    override fun getItem(position: Int): Any {
        return shoppingList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.item1_shop, parent, false)
        val list = getItem(position) as ShoppingBasket

        val name = rowView.findViewById(R.id.listName) as TextView
        val available = rowView.findViewById(R.id.available) as TextView
        val deleteButton = rowView.findViewById(R.id.deleteItem) as ImageButton
        name.text = list.name
        available.text = list.quantity

        deleteButton.setOnClickListener {
            MaterialAlertDialogBuilder(context)
                .setTitle("Delete Item")
                .setMessage("Are you sure you want to remove this item")
                .setNegativeButton("Decline") { dialog, which ->
                    // Respond to negative button press
                }
                .setPositiveButton("Accept") { dialog, which ->
                    Coroutines.io {
                        repository.deleteShopping(list.id!!)
                    }
                    Toast.makeText(context, "Item removed", Toast.LENGTH_SHORT).show()
                    // Respond to positive button press
                }
                .show()
        }

        return rowView
    }

    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                bugsFiltered = if (charString.isNullOrEmpty()) {
                    val filteredList: ArrayList<ShoppingBasket> = ArrayList()
                    for (row in mRecyclerViewItems1!!) {
                        filteredList.add(row)
                    }
                    filteredList
                } else {
                    val filteredList: ArrayList<ShoppingBasket> = ArrayList()
                    for (row in mRecyclerViewItems1!!) {

                        if (row.name.toLowerCase()
                                .contains(charString.toLowerCase(Locale.ROOT))) {
                            filteredList.add(row)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = bugsFiltered
                return filterResults
            }
            @Suppress("UNCHECKED_CAST")
            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                mRecyclerViewItems!!.clear()
                mRecyclerViewItems!!.addAll(filterResults.values as ArrayList<ShoppingBasket>)
                notifyDataSetChanged()
            }
        }
    }
}