package com.kvng_willy.applaunch.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket

@Dao
interface ShoppingDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(shoppingBasket: ShoppingBasket) : Long

    @Query("SELECT * FROM ShoppingBasket")
    fun getShoppingItem() : LiveData<List<ShoppingBasket>>

    @Query("DELETE FROM ShoppingBasket WHERE id=:uid")
    suspend fun delete(uid:Int)
}