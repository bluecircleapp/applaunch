package com.kvng_willy.applaunch.ui

import androidx.lifecycle.LiveData
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket

interface Listener {
    fun onTaskReturn(message: LiveData<String>?)
}