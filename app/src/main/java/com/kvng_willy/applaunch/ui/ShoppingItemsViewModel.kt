package com.kvng_willy.applaunch.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket
import com.kvng_willy.applaunch.data.network.ShoppingList
import com.kvng_willy.applaunch.data.preferences.PreferenceProvider
import com.kvng_willy.applaunch.data.repositories.DataRepository

class ShoppingItemsViewModel(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
) : ViewModel() {

    var listener:Listener? = null

    var shoppingList = repository.getShopping()

}