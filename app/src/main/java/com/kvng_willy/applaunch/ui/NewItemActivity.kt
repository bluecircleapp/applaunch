package com.kvng_willy.applaunch.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.kvng_willy.applaunch.R
import com.kvng_willy.applaunch.databinding.ActivityDetailBinding
import com.kvng_willy.applaunch.databinding.ActivityNewItemBinding
import com.kvng_willy.applaunch.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class NewItemActivity : AppCompatActivity(),KodeinAware,Listener {
    override val kodein by kodein()
    private val factory: GenericModelFactory by instance()
    private lateinit var viewmodel: GenericViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val binding: ActivityNewItemBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_new_item
        )
        viewmodel = ViewModelProvider(this, factory).get(GenericViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.listener = this
        binding.lifecycleOwner = this

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onTaskReturn(message: LiveData<String>?) {
        toast("Added to cart")
    }
}