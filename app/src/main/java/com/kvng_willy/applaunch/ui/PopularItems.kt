package com.kvng_willy.applaunch.ui

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.kvng_willy.applaunch.R
import com.kvng_willy.applaunch.adapter.ShoppingAdapter
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket
import com.kvng_willy.applaunch.databinding.PopularItemsFragmentBinding
import com.kvng_willy.applaunch.util.ApiException
import com.kvng_willy.applaunch.util.NoInternetException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class PopularItems : Fragment(),KodeinAware,Listener {

    companion object {
        fun newInstance() = PopularItems()
    }
    private lateinit var binding: PopularItemsFragmentBinding
    override val kodein by kodein()

    private val factory: PopularItemsModelFactory by instance()
    private lateinit var viewmodel: PopularItemsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.popular_items_fragment, container, false)
        binding.lifecycleOwner = this

        return binding.root
    }

   /* override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewmodel =
            ViewModelProvider(this,factory).get(PopularItemsViewModel::class.java)
        binding.viewmodel = viewmodel

    }*/

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewmodel =
            ViewModelProvider(this,factory).get(PopularItemsViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.listener = this

        viewmodel.fetchShoppingList()
    }

    override fun onTaskReturn(message: LiveData<String>?) {
        message?.observe(this, {
            if (it.equals("an error occurred", ignoreCase = true)) {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            } else {
                val shoppingList = viewmodel.getSavedData(it)
                val adapter = ShoppingAdapter(requireContext(), shoppingList)
                binding.listView.adapter = adapter

                binding.listView.setOnItemClickListener { _: AdapterView<*>, _: View, i: Int, _: Long ->
                    val selectedShopping = shoppingList[i]
                    val detailIntent = DetailActivity.newIntent(requireContext(), selectedShopping)
                    startActivity(detailIntent)
                }
            }
        })

    }
}