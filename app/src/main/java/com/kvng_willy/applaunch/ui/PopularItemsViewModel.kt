package com.kvng_willy.applaunch.ui

import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.kvng_willy.applaunch.data.db.entities.ShoppingBasket
import com.kvng_willy.applaunch.data.network.ShoppingList
import com.kvng_willy.applaunch.data.preferences.PreferenceProvider
import com.kvng_willy.applaunch.data.repositories.DataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray

class PopularItemsViewModel(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
) : ViewModel() {

    var listener: Listener? = null

    fun fetchShoppingList(){
        val jsonResponse = repository.getShoppingList()
        listener?.onTaskReturn(jsonResponse)
    }

    fun getSavedData(json:String):List<ShoppingList> {
        return Gson().fromJson(json, Array<ShoppingList>::class.java).asList()
    }
}