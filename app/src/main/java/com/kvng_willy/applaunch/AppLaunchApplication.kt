package com.kvng_willy.applaunch

import androidx.multidex.MultiDexApplication
import com.kvng_willy.applaunch.data.db.AppDatabase
import com.kvng_willy.applaunch.data.network.MyApi
import com.kvng_willy.applaunch.data.network.NetworkConnectionInterceptor
import com.kvng_willy.applaunch.data.preferences.PreferenceProvider
import com.kvng_willy.applaunch.data.repositories.DataRepository
import com.kvng_willy.applaunch.ui.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class AppLaunchApplication:MultiDexApplication(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@AppLaunchApplication))

        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { DataRepository(instance(),instance()) }
        bind() from provider { ShoppingItemsModelFactory(instance(),instance()) }
        bind() from provider { PopularItemsModelFactory(instance(),instance()) }
        bind() from provider { GenericModelFactory(instance(),instance()) }
    }

}